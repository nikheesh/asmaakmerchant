package com.asmaakmerchant.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout {

	// public static boolean mMapIsTouched = false;

	public TouchableWrapper(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public TouchableWrapper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
		AsmaakApplication.mMapIsTouched = true;
			break;

		case MotionEvent.ACTION_UP:
			AsmaakApplication.mMapIsTouched = false;
			break;

		}

		return super.dispatchTouchEvent(ev);
	}
}
