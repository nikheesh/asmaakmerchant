package com.asmaakmerchant.app;



import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainTabDelivery extends FragmentActivity {

	// Declaring Your View and Variables
   
	// Toolbar toolbar;
	ViewPager pager;
	PageAdapterDelivery adapter;
	SlidingTabLayout tabs;
ImageButton imgbtn_back;
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.main_tab_trip);
		
		
		
		 imgbtn_back = (ImageButton) findViewById(R.id.left_arrow);
	imgbtn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			}
		});
		// Intent intent = getIntent();
		// Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
		// linear_contact=(LinearLayout)findViewById(R.id.linear_contact);
		// BitmapDrawable background = new BitmapDrawable();
		//
		// linear_contact.
	CharSequence Titles[] = { "New Order", "On Delivery","Delivered","Delivery places" };
	int Numboftabs = 4;

	// Creating The ViewPagerAdapter and Passing Fragment Manager, Titles
	// fot the Tabs and Number Of Tabs.
	adapter = new PageAdapterDelivery(getSupportFragmentManager(), Titles,
			Numboftabs);

	// Assigning ViewPager View and setting the adapter
	pager = (ViewPager) findViewById(R.id.pager_trip);
	pager.setAdapter(adapter);

	// Assiging the Sliding Tab Layout View
	tabs = (SlidingTabLayout) findViewById(R.id.sliding_tabs_trip);

	tabs.setViewPager(pager);

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//
//		// noinspection SimplifiableIfStatement
//		if (id == R.id.action_settings) {
//			return true;
//		}

		return super.onOptionsItemSelected(item);
	}
	@Override
	protected void onResume() {

		super.onResume();
		
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}

	
}