package com.asmaakmerchant.app;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

public class Delivered_Fragment extends Fragment {

	ArrayAdapter<DeliveryClass> mytripAdapter;

	ListView list;
	EditText search;
	DeliveryClass contclass;
	
	
	ProgressWheel progressdialog;
	TextView txt_noitems;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View contacts = inflater.inflate(R.layout.delivered, container,
				false);
		return contacts;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mytripAdapter = new ArrayAdapter<DeliveryClass>(getActivity(), 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = getActivity().getLayoutInflater().inflate(
							R.layout.mytrip_item, null);
				}

				final DeliveryClass cont = this.getItem(position);
				try {
					TextView txt_address = (TextView) convertView
							.findViewById(R.id.txt_deliveryitem_address);
					txt_address.setText(cont.address);
					
					TextView txt_phone = (TextView) convertView
							.findViewById(R.id.txt_deliveryitem_phone);
					txt_phone.setText(cont.contact);
					
					TextView txt_billamount = (TextView) convertView
							.findViewById(R.id.txt_deliveryitem_billamount);
					txt_billamount.setText("QR "+cont.billAmount);
					
					TextView txt_paymentstatus = (TextView) convertView
							.findViewById(R.id.txt_deliveryitem_paymentstatus);
		
					
					TextView txt_usertype = (TextView) convertView
							.findViewById(R.id.txt_deliveryitem_usertype);
					txt_usertype.setText(cont.usertype);
					
					Button viewdetails = (Button) convertView
							.findViewById(R.id.btn_deliveryitem_viewdetails);
					
					if(cont.isBillpayed)
						txt_paymentstatus.setText("payed");
						else
							txt_paymentstatus.setText("not payed");
						
					
					viewdetails.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent i=new Intent(getActivity(), DeliveryInDetail_Activity.class);
							i.putExtra("obid", cont.obid);
							i.putExtra("address", cont.address);
							i.putExtra("phone", cont.contact);
							i.putExtra("billAmount", cont.billAmount);
							i.putExtra("isBillPayed", cont.isBillpayed);
							i.putExtra("usertype", cont.usertype);
							i.putExtra("isDeliver", cont.deliverstatus);
							
							startActivity(i);

						}
					});

				} catch (Exception e) {
				}

				// text.setText(tweet.get("mdesc").getAsString());
				return convertView;
			}
		};

		list = (ListView) getActivity().findViewById(R.id.listview_delivered_list);
		list.setAdapter(mytripAdapter);
		
		progressdialog=(ProgressWheel)getActivity().findViewById(R.id.progress_delivered_progress);
		txt_noitems=(TextView)getActivity().findViewById(R.id.txt_delivered_noitems);
		
		

	}

	@Override
	public void onResume() {

		super.onResume();
		
		mytripAdapter.clear();
		mytripAdapter.notifyDataSetChanged();
		
		txt_noitems.setVisibility(View.GONE);
		list.setVisibility(View.GONE);
		progressdialog.setVisibility(View.VISIBLE);
		progressdialog.spin();
		

		ParseCloud.callFunctionInBackground("getDeliveredOrdersByDate_Merchant",
				new HashMap<String, Object>(),
				new FunctionCallback<List<ParseObject>>() {

					@Override
					public void done(List<ParseObject> result, ParseException e) {
						// TODO Auto-generated method stub
						progressdialog.stopSpinning();
						progressdialog.setVisibility(View.GONE);
					
						if (e == null) {
							// result is "Hello world!"
							// Toast.makeText(getActivity(),
							// "successfully loaded", Toast.LENGTH_LONG).show();
							if(result.size()>0){
								txt_noitems.setVisibility(View.GONE);
								list.setVisibility(View.VISIBLE);
								
							
							
							for (int i = 0; i < result.size(); i++) {

								ParseObject order = result.get(i);
										

								contclass = new DeliveryClass();
								
								
								try{
								contclass.address = order
										.getString("delivery_address");
								}
								catch(Exception e1){
									
								}
								try{
								contclass.contact = order
										.getString("contact_number");
								}
								catch(Exception e2){
									
								}
								
								contclass.deliverstatus=2;
								
								
								try{
								contclass.billAmount = order.getNumber("total_cost").toString();
								}
								catch(Exception e4){
									
								}
								try{
								contclass.isBillpayed = order.getBoolean("isPaymentSuccess");}
								catch(Exception e5){}
								try{
									boolean isguest=false;
									isguest=order.getBoolean("isGuestUser");
									if(!isguest)
									
								contclass.usertype = "Registered User";
									else
										contclass.usertype="UnRegitered User";
								}
								catch(Exception e6){}
								contclass.obid=order.getObjectId();
								
								mytripAdapter.add(contclass);
								
								

							}
							
							
							}
							
							else{
								progressdialog.setVisibility(View.GONE);
							
								txt_noitems.setVisibility(View.VISIBLE);
							
								list.setVisibility(View.GONE);
							}
						}
						
						else {
							progressdialog.stopSpinning();
							progressdialog.setVisibility(View.GONE);
						
							txt_noitems.setVisibility(View.VISIBLE);
							txt_noitems.setText("Please check your internet connection");
							list.setVisibility(View.GONE);
							
						}
							
							
						
					}
				});
		
	}
	
	
}
