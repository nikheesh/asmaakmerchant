package com.asmaakmerchant.app;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;


public class DeliveryPlaces_Fragment extends Fragment {

	SupportMapFragment fragment;

	// private ArrayList<HashMap<Double, Double>> geopints = new ArrayList<HashMap<Double, Double>>();

	
	
	boolean isDrawroute = false;
ArrayList<LatLng> listmarkers;
ArrayList<String> listaddress;
ArrayList<Marker> markers;

	GoogleMap googleMap;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	Bundle savedInstanceState) {
		View contacts = inflater.inflate(R.layout.activity_deliveryplaces, container, false);
		return contacts;
	}


	@SuppressLint("SimpleDateFormat")@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		
		
		this.getView().setFocusableInTouchMode(true);

		this.getView().setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					
					getActivity().finish();
					
					return true;
				}
				return false;
			}
		});
		
		
		FragmentManager fm = getFragmentManager();
		fragment = (SupportMapFragment) fm.findFragmentById(R.id.from_map);
		if (fragment == null) {
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.from_map, fragment).commit();
		}
		listmarkers=new  ArrayList<LatLng>();
		listaddress=new ArrayList<String>();
		markers=new ArrayList<Marker>();
		// Getting GoogleMap from SupportMapFragment
		googleMap = fragment.getMap();
		CameraUpdate cameraZoom = CameraUpdateFactory.zoomBy(5);

		// Showing the user input location in the Google Map

		googleMap.animateCamera(cameraZoom);
		googleMap.setMyLocationEnabled(true);
//		try{
//		 Criteria criteria = new Criteria();
//		    LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//		    String provider = locationManager.getBestProvider(criteria, false);
//		    Location location = locationManager.getLastKnownLocation(provider);
//		    double lat =  location.getLatitude();
//		    double lng = location.getLongitude();
//		    LatLng coordinate = new LatLng(lat, lng);
//		    CameraUpdate center=
//		            CameraUpdateFactory.newLatLng(new LatLng(lat,
//		                                                     lng));
//		        CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
//
//		        googleMap.moveCamera(center);
//		        googleMap.animateCamera(zoom);
//
//		}
//		catch(Exception e){
//
//		}
//		
		
		
		
		ParseCloud.callFunctionInBackground("getOnDeliveryOrdersByDate_Merchant",
				new HashMap<String, Object>(),
				new FunctionCallback<List<ParseObject>>() {

					@Override
					public void done(List<ParseObject> result, ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							// result is "Hello world!"
							// Toast.makeText(getActivity(),
							// "successfully loaded", Toast.LENGTH_LONG).show();
							for (int i = 0; i < result.size(); i++) {
								try{
								ParseObject order = result.get(i);
								
								ParseGeoPoint pg=order.getParseGeoPoint("user_geopoint");
								
								LatLng latlng=new LatLng(pg.getLatitude(), pg.getLongitude());
								listmarkers.add(latlng);
								listaddress.add(order.getString("delivery_address"));
								
								
								
								Log.e("list of points",latlng.latitude+","+latlng.longitude);
								
								Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.mapicon);
								Bitmap mapicon=Bitmap.createScaledBitmap(b, 40,40, false);
								
							Marker marker=	googleMap
								.addMarker(
										new MarkerOptions()
												.position(latlng)
												.title(""+latlng)
												.icon(BitmapDescriptorFactory.fromBitmap(mapicon)));

							
							markers.add(marker);
							
								
								}
								catch(Exception e1){
									
								}
								
								
							}
							try{
							LatLngBounds.Builder b = new LatLngBounds.Builder();
							for (Marker m : markers) {
								b.include(m.getPosition());
							}
							LatLngBounds bounds = b.build();
							// Change the padding as per needed
							CameraUpdate cu = CameraUpdateFactory
									.newLatLngBounds(bounds, 100, 100,
											1);

							googleMap.animateCamera(cu);

							}
							catch(Exception e2){
								
							}
							
						}
					}
				});

		googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {
			 
            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }
 
            // Defines the contents of the InfoWindow
           
			@Override
            public View getInfoContents(final Marker marker) {
 
                // Getting view from the layout file info_window_layout
                View v = getActivity().getLayoutInflater().inflate(R.layout.infowindow_googlemarker, null);
 
                // Getting the position from the marker
              final  LatLng latLng = marker.getPosition();
 
                // Getting reference to the TextView to set latitude
                TextView txt_lat = (TextView) v.findViewById(R.id.txt_info_address1);
                txt_lat.setText(getAddress(latLng.latitude, latLng.longitude));
                
                TextView txt_original=(TextView)v.findViewById(R.id.txt_info_address2);
                // Getting reference to the TextView to set longitude
                  
                  for(int i=0;i<listmarkers.size();i++){
                  if(listmarkers.get(i).equals(latLng))
                  	{
                	  txt_original.setText(listaddress.get(i));
                	  break;
                  
                  	}
                  }
                   
                  
                  // Returning the view containing InfoWindow contents
                return v;
 
            }
        });
		
 googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
	
	@Override
	public void onInfoWindowClick(Marker m) {
		try{
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
			    Uri.parse("http://maps.google.com/maps?daddr="+m.getPosition().latitude+","+m.getPosition().longitude));
			startActivity(intent);
		}
		catch(Exception e){
			
		}
		
	}
});
		
	
	}

	private String getAddress(double latitude, double longitude) {
		StringBuilder result = new StringBuilder();
		try {
			Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
			List < Address > addresses = geocoder.getFromLocation(latitude, longitude, 1);
			if (addresses.size() > 0) {
				Address address = addresses.get(0);
				result.append(address.getLocality()).append(",");
				result.append(address.getCountryName());
			}
		} catch (IOException e) {
			Log.e("tag", e.getMessage());
		}

		return result.toString();
	}

	

	
	@Override
	public void onDestroyView() {
		 
	    Fragment fragment = (getFragmentManager().findFragmentById(R.id.from_map));  
	    if(fragment!=null){
	    if (fragment.isResumed()) {
	    	 getFragmentManager().beginTransaction().remove(fragment).commit();

//	    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//	    ft.remove(fragment);
//	    ft.commit();
	    }}
	    
	    super.onDestroyView();
		
	    
//		FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//		ft.remove(fragment);
//		ft.commit();
	}

}