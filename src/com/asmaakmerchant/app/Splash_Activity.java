package com.asmaakmerchant.app;



import com.parse.ParseUser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;


public class Splash_Activity extends Activity {
	
	ParseUser user;
	
	private boolean isBackPressed = false;
	private static int SPLASH_TIME_OUT=1500;
  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		user=ParseUser.getCurrentUser();
		 
				new Handler().postDelayed(new Runnable(){
					public void run()
					{   if(!isBackPressed)
					{
						if(user==null){
							finish();
							Intent i=new Intent(getApplicationContext(),Login.class);
							startActivity(i);
						}
						else{
							finish();
						Intent i=new Intent(getApplicationContext(),Home_Activity.class);
						startActivity(i);
						}
					}}
				},SPLASH_TIME_OUT);
			
	}
       
    
	
	 @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	            isBackPressed = true;
	            finish();
	        }
	        return super.onKeyDown(keyCode, event);

	    }
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
			}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	
	
	}

