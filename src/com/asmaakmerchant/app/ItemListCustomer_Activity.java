package com.asmaakmerchant.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;


import android.app.Activity;
import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ItemListCustomer_Activity extends Activity {
	
	ArrayAdapter<ItemClass> contactAdapter;
	GridView list;
	//Button btn_getorder;
	ProgressWheel progressdialog;
	TextView txt_noitems;
	ImageButton imgbtn_back;
	
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)
	.showImageForEmptyUri(R.drawable.ic_launcher)
	 .showImageOnFail(R.drawable.ic_launcher)
	 .showImageOnLoading(R.drawable.ic_launcher)
	.build();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		
		contactAdapter = new ArrayAdapter<ItemClass>(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = getLayoutInflater().inflate(
							R.layout.items_item, null);
				}
 
				final ItemClass it=getItem(position);

				ImageView imageView = (ImageView) convertView
							.findViewById(R.id.img_item_itemimg);
					
					TextView txt_name = (TextView) convertView
							.findViewById(R.id.txt_item_name);
					//TextView txt_bestfor=(TextView)convertView.findViewById(R.id.txt_item_bestfor);
					TextView txt_fresness=(TextView)convertView.findViewById(R.id.txt_item_freshnessscale);
					TextView txt_price=(TextView)convertView.findViewById(R.id.txt_item_price);
					TextView txt_cleaning_charge=(TextView)convertView.findViewById(R.id.txt_item_cleaningcharge);
					ImageLoader imageLoader = ImageLoader.getInstance();
					LinearLayout ll=(LinearLayout)convertView.findViewById(R.id.ll_item_layoutall);
				//	Button btn_ordernow=(Button)convertView.findViewById(R.id.btn_item_ordernow);
					txt_name.setText(it.name);
					txt_price.setText("QR "+it.price+"/KG");
					txt_fresness.setText("Freshness scale : "+it.freshnes_scale);
					txt_cleaning_charge.setText("Processing charge : "+it.processing_charge);
				//	txt_fresness.setText(it.freshnes_scale);

					if (it.img != null) {
						imageLoader.displayImage(it.img, imageView,options);

					} else {
						imageView.setImageResource(R.drawable.ic_launcher);
					}
					
					ll.setOnClickListener(new  OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							Intent i=new Intent(getApplicationContext(),EditItem.class);
							i.putExtra("name", it.name);
							i.putExtra("processingcharge", it.processing_charge);
							i.putExtra("freshness_scale", it.freshnes_scale);
							i.putExtra("price", it.price);
							i.putExtra("itemid", it.obid);
							i.putExtra("imageurl",it.img );
							i.putExtra("uom", it.uom_type);
							i.putExtra("fish_desc", it.description);
							i.putExtra("processings", it.processings);
							i.putExtra("isAvailable", it.isAvailable);
							
							startActivity(i);
							
						}
					});

				return convertView;
			}
		};

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		
		
		setContentView(R.layout.activity_itemlist_customer);
		list = (GridView)findViewById(R.id.gridview_item_customer);
		list.setAdapter(contactAdapter);
		//btn_getorder=(Button)findViewById(R.id.btn_item_getorder);
		progressdialog=(ProgressWheel)findViewById(R.id.progress_item_progress);
		txt_noitems=(TextView)findViewById(R.id.txt_item_noitems);
		imgbtn_back=(ImageButton)findViewById(R.id.imgbtn_common_back);
		imgbtn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			finish();
			
			overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			}
		});
		
	}
	
	
	@Override
	protected void onResume() {

		super.onResume();
	
		txt_noitems.setVisibility(View.GONE);
		list.setVisibility(View.GONE);
		progressdialog.setVisibility(View.VISIBLE);
		progressdialog.spin();
		
		contactAdapter.clear();
		contactAdapter.notifyDataSetChanged();
		
//		btn_getorder.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//			Intent i=new Intent(getApplicationContext(), MainTabDelivery.class);
//			startActivity(i);
//			}
//		});
//		
//		
		HashMap<String, String> input_param = new HashMap<String, String>();
		String category;
		
		ParseCloud.callFunctionInBackground("getAllFishes_Merchant", input_param,
				new FunctionCallback<List<ParseObject>>() {

					@Override
					public void done(List<ParseObject> result,
							ParseException e) {
						// TODO Auto-generated method stub
						progressdialog.stopSpinning();
						progressdialog.setVisibility(View.GONE);
						if (e == null) {
							if(result.size()>0){
								txt_noitems.setVisibility(View.GONE);
								list.setVisibility(View.VISIBLE);
								
							for(int i=0;i<result.size();i++){
								ParseObject item=result.get(i);
								ItemClass itclass=new ItemClass();
								itclass.name=item.getString("fish_name");
								try{
								itclass.img=item.getParseFile("fish_img").getUrl();
								}
								catch(Exception e1){
									
								}
								itclass.obid=item.getObjectId();
								itclass.description=item.getString("fish_desc");
								itclass.freshnes_scale=item.getString("freshness_scale");
								itclass.processing_charge=item.getNumber("processing_charge").toString();
								itclass.bestfor=item.getString("best_for");
								itclass.price=item.getNumber("unit_price").toString();
								itclass.uom_type=item.getString("uom");
								itclass.isAvailable=item.getBoolean("isAvailable");
								
								ArrayList<String> processings=new  ArrayList<String>();
								for(int j=0;j<item.getJSONArray("processings").length();j++)
								{
									try {
										processings.add(item.getJSONArray("processings").getString(j));
									} catch (JSONException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
								itclass.processings=processings;
								
								
								
								contactAdapter.add(itclass);
								Log.e("retrieved object", item.toString());
							}
							
						
						
						
						}
						else{
							
							txt_noitems.setVisibility(View.VISIBLE);
						
							list.setVisibility(View.GONE);
						}
					
					}
						else {
							
							txt_noitems.setVisibility(View.VISIBLE);
							txt_noitems.setText("Please check your internet connection");
							list.setVisibility(View.GONE);
							
						}
						
					
					
					}
					
		
		});
		
		
	
	}

	
}
