package com.asmaakmerchant.app;



import com.parse.ParseUser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;


public class Home_Activity extends Activity {
	
	ParseUser user;
	RelativeLayout btn_itemlist,btn_orders,btn_signout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		btn_itemlist=(RelativeLayout)findViewById(R.id.rl_home_itemlist);
		btn_orders=(RelativeLayout)findViewById(R.id.rl_home_orders);
		btn_signout=(RelativeLayout)findViewById(R.id.rl_home_signout);
		
		btn_itemlist.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(), ItemListCustomer_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			}
		});
		
		btn_orders.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				
				Intent i=new Intent(getApplicationContext(), MainTabDelivery.class);
				startActivity(i);
				overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			}
		});
		
		btn_signout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				ParseUser.logOut();
				finish();
				Intent i=new Intent(getApplicationContext(), Login.class);
				startActivity(i);
				overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
			}
		});
	}
       
    
	
	
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
			}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	
	
	}

