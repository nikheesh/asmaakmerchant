package com.asmaakmerchant.app;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.WindowManager.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Utilities_Functions {
	public static ProgressDialog proDialog;
	public static void startLoading(Context context) {
	    
		proDialog = new ProgressDialog(context);
	    proDialog.setMessage("Signing in. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

public static void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	
	public static boolean checkinternet(Context context){
		 
		 ConnectivityManager   conmgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo ni = conmgr.getActiveNetworkInfo();
			if (ni == null) {
			    // There are no active networks.
			    return false;
			}else{
				boolean isConnected = ni.isConnected();
				return isConnected;
			}
		
	}
	
	//to check user input email 
	public static boolean chekEmailId(String email){
		
		 boolean isValid = false;

		    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		    CharSequence inputStr = email;

		    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		    Matcher matcher = pattern.matcher(inputStr);
		    if (matcher.matches()) {
		        isValid = true;
		    }
		    return isValid;
		
	}

	//for wraping listview inside scroll view
	
		public static void setListViewHeightBasedOnChildren(ListView listView) {
		    ListAdapter listAdapter = listView.getAdapter();
		    if (listAdapter == null)
		        return;

		    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		    int totalHeight = 0;
		    View view = null;
		    for (int i = 0; i < listAdapter.getCount(); i++) {
		        view = listAdapter.getView(i, view, listView);
		        if (i == 0)
		            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

		        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
		        totalHeight += view.getMeasuredHeight();
		    }
		    ViewGroup.LayoutParams params = listView.getLayoutParams();
		    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		    listView.setLayoutParams(params);
		    listView.requestLayout();
		}
		
		public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

		    Matrix matrix = new Matrix();
		    switch (orientation) {
		        case ExifInterface.ORIENTATION_NORMAL:
		            return bitmap;
		        case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
		            matrix.setScale(-1, 1);
		            break;
		        case ExifInterface.ORIENTATION_ROTATE_180:
		            matrix.setRotate(180);
		            break;
		        case ExifInterface.ORIENTATION_FLIP_VERTICAL:
		            matrix.setRotate(180);
		            matrix.postScale(-1, 1);
		            break;
		        case ExifInterface.ORIENTATION_TRANSPOSE:
		            matrix.setRotate(90);
		            matrix.postScale(-1, 1);
		            break;
		       case ExifInterface.ORIENTATION_ROTATE_90:
		           matrix.setRotate(90);
		           break;
		       case ExifInterface.ORIENTATION_TRANSVERSE:
		           matrix.setRotate(-90);
		           matrix.postScale(-1, 1);
		           break;
		       case ExifInterface.ORIENTATION_ROTATE_270:
		           matrix.setRotate(-90);
		           break;
		       default:
		           return bitmap;
		    }
		    try {
		        Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		        bitmap.recycle();
		        return bmRotated;
		    }
		    catch (OutOfMemoryError e) {
		        e.printStackTrace();
		        return null;
		    }
		}

	
}
