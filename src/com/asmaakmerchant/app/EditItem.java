package com.asmaakmerchant.app;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import android.R.string;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class EditItem	extends Activity {
	
	EditText edit_name,edit_freshness,edit_price,edit_processingcharge,edit_description,edit_uom,edit_uom2;
	
	Spinner spinner_available;
	//Spinner cleaningstatus;
	Button btn_save;
	ImageView item_img;
	ArrayAdapter<Processing_Class> adapterProcess;
	ImageButton img_back;
	
	
	byte[] array;
	final int SELECT_PHOTO = 1;

	ArrayList<String> defaultprocessnames;
	ArrayList<String> processingslist;
	
	protected ProgressDialog proDialog;

	protected void startLoading() {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage("Please Wait.....");
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	protected void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	
	
	// for quantity increment
	//Button btn_add_qty,btn_sub_qty;
	//int quantity=1;
	ArrayList<String> avialableList=new ArrayList<String>();
	ArrayAdapter<String> availableAdapter;
	
	boolean isAvailable=false;
	
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)
	// .showImageForEmptyUri(fallback)
	// .showImageOnFail(fallback)
	// .showImageOnLoading(fallback)
	.build();
	
	ParseUser user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		
		adapterProcess=new  ArrayAdapter<Processing_Class>(this, 0){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = getLayoutInflater().inflate(R.layout.processing_item, null);
				}
				final Processing_Class process_class=this.getItem(position);
				TextView name=(TextView)convertView.findViewById(R.id.txt_processingitem_processname);
				ImageButton check=(ImageButton)convertView.findViewById(R.id.imgbtn_processingitem_check);
				
				name.setText(process_class.processname);
				if(process_class.check)
				check.setImageResource(R.drawable.near_visible);
				else
					check.setImageResource(R.drawable.near_notvisible);
				
				
				check.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
					
						process_class.check=!process_class.check;
						
						adapterProcess.notifyDataSetChanged();
					}
				});
				
				
				
			return convertView;
		}};
		
		
		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_edititem);
		
		user=ParseUser.getCurrentUser();
			   
		edit_name=(EditText)findViewById(R.id.edit_edititem_name);
		edit_freshness=(EditText)findViewById(R.id.edit_edititem_freshness);
		edit_price=(EditText)findViewById(R.id.edit_edititem_price);
		edit_processingcharge=(EditText)findViewById(R.id.edit_edititem_processingcharge);
		edit_description=(EditText)findViewById(R.id.edit_edititem_description);
		edit_uom=(EditText)findViewById(R.id.edit_edititem_uom);
		edit_uom2=(EditText)findViewById(R.id.edit_edititem_uom2);
		//edit_address=(EditText)findViewById(R.id.edit_edititem_address);
		//edit_qty=(EditText)findViewById(R.id.edit_edititem_quantity);
		item_img=(ImageView)findViewById(R.id.img_edititem_itemimg);
		//cleaningstatus=(Spinner)findViewById(R.id.spinner_edititem_cleaning);
		img_back=(ImageButton)findViewById(R.id.imgbtn_common_back);
		spinner_available=(Spinner)findViewById(R.id.spinner_edititem_available);
		
		ListView list=(ListView)findViewById(R.id.listview_edititem_processing);
		list.setAdapter(adapterProcess);
		Utilities_Functions.setListViewHeightBasedOnChildren(list);
		
	
		avialableList.add("Yes");
		avialableList.add("No");
		availableAdapter=new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, avialableList);
		
		spinner_available.setAdapter(availableAdapter);
		
		
			
		btn_save=(Button)findViewById(R.id.btn_edititem_edititembtn);
		
		img_back.setOnClickListener(new  OnClickListener() {
			
			@Override
			public void onClick(View v) {
			finish();	
			}
		});
	//	i.putExtra("name", it.name);
//		i.putExtra("cleaningcharge", it.cleaning_charge);
//		i.putExtra("freshness_scale", it.freshnes_scale);
//		i.putExtra("price", it.price);
//		i.putExtra("itemid", it.obid);
	//i.putExtra("imageurl",it.img );	
	final	String name=getIntent().getStringExtra("name");
	final	String process_chg=getIntent().getStringExtra("processingcharge");
	final	String freshness=getIntent().getStringExtra("freshness_scale");
	final	String price=getIntent().getStringExtra("price");
	final	String itemid=getIntent().getStringExtra("itemid");
	final String imgurl=getIntent().getStringExtra("imageurl");
	final String uom=getIntent().getStringExtra("uom");
	final String fish_desc=getIntent().getStringExtra("fish_desc");
	final boolean isavailable=getIntent().getBooleanExtra("isAvailable", false);
	
	
	
	ImageLoader  imgloader=ImageLoader.getInstance();
		imgloader.displayImage(imgurl, item_img);
	//edit_cleaning.setText(clean_chg);
	edit_freshness.setText(freshness);
	edit_processingcharge.setText(process_chg);
	edit_name.setText(name);
	edit_price.setText(""+price);
	edit_description.setText(fish_desc);
	edit_uom.setText("/ "+uom);
	edit_uom2.setText("/ "+uom);
	item_img.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
		
			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			startActivityForResult(photoPickerIntent, SELECT_PHOTO);

		}
	});
	if(isavailable)
	spinner_available.setSelection(0);
	else
		spinner_available.setSelection(1);
	processingslist=new ArrayList<String>();
	processingslist=getIntent().getStringArrayListExtra("processings");
			
	defaultprocessnames=new ArrayList<String>();
	defaultprocessnames.add("Cleaned");
	defaultprocessnames.add("Choped");
	defaultprocessnames.add("Fillet");
	
	
	for(int i=0;i<defaultprocessnames.size();i++){
		
		Processing_Class proc=new Processing_Class();
		proc.processname=defaultprocessnames.get(i);
		proc.check=false;
		for(int j=0;j<processingslist.size();j++){
			if(processingslist.get(j).equalsIgnoreCase(defaultprocessnames.get(i))){
				proc.check=true;
				processingslist.remove(j);
				break;
			}
		}
		
		adapterProcess.add(proc);
		
	}
	Utilities_Functions.setListViewHeightBasedOnChildren(list);
		btn_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
		
//			final Map<String, Object> inputparm = new HashMap<String, Object>();
//			inputparm.put("freshness", edit_freshness.getText().toString());
//			inputparm.put("processingcharge", edit_processingcharge.getText().toString());
//			inputparm.put("unit_price", edit_price.getText().toString());
//			inputparm.put("fish_id", itemid);
		
			if(spinner_available.getSelectedItem().equals("Yes")){
				isAvailable=true;
			}
			
			startLoading();
			
			if(array!=null){

				final ParseFile pf = new ParseFile("image.jpeg", array);
				pf.saveInBackground(new SaveCallback() {

					@Override
					public void done(ParseException e) {
						
						
						if (e == null) {
							//inputparm.put("fish_img", pf);
							
							ParseQuery<ParseObject> query = ParseQuery.getQuery("Fish");
							query.getInBackground(itemid, new GetCallback<ParseObject>() {
							  public void done(ParseObject fish, ParseException e) {
							    if (e == null) {
							    	
							    	
							    fish.put("freshness_scale",edit_freshness.getText().toString());
							    fish.put("isAvailable", isAvailable);
							    fish.put("fish_name",edit_name.getText().toString());
							    fish.put("unit_price", Float.parseFloat(edit_price.getText().toString()));
							    fish.put("processing_charge",Float.parseFloat(edit_processingcharge.getText().toString()));
							    fish.put("fish_img", pf);
							    
							    ArrayList<String> listmarked=new ArrayList<String>();
							    listmarked.add("Not Cleaned");
							    for(int i=0;i<adapterProcess.getCount();i++){
							    	if(adapterProcess.getItem(i).check)
							    	{
							    		listmarked.add(adapterProcess.getItem(i).processname);
							    	}
							    	
							    }
							    
							    fish.put("processings",listmarked);
							    Log.e("list marked",listmarked.toString());
							    
							    fish.saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										if(e==null)
										{
											
											if(proDialog!=null)
												stopLoading();
										finish();
										Toast.makeText(getApplicationContext(), "Successfully updated the status", Toast.LENGTH_LONG).show();
										}
										else{
											if(proDialog!=null)
												stopLoading();
											Toast.makeText(getApplicationContext(), "Unable to Update the fish details now", Toast.LENGTH_LONG).show();
										    
										}
										
									}
								});
							    
							    
							    
							    
							    } else {
							    	if(proDialog!=null)
										stopLoading();
							    	Toast.makeText(getApplicationContext(), "Unable to Update the fish details now", Toast.LENGTH_LONG).show();
							      // something went wrong
							    }
							  }
							});
		
							
							
						}}});
				
			}
			else{
				//inputparm.put("fish_img", null);
				
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Fish");
				query.getInBackground(itemid, new GetCallback<ParseObject>() {
				  public void done(ParseObject fish, ParseException e) {
				    if (e == null) {
				    fish.put("freshness_scale",edit_freshness.getText().toString());
				    fish.put("isAvailable", isAvailable);
				    fish.put("fish_name",edit_name.getText().toString());
					  
				    fish.put("unit_price", Float.parseFloat(edit_price.getText().toString()));
				    fish.put("processing_charge",Float.parseFloat(edit_processingcharge.getText().toString()));
				    
				    ArrayList<String> listmarked=new ArrayList<String>();
				    listmarked.add("Not Cleaned");
				    for(int i=0;i<adapterProcess.getCount();i++){
				    	if(adapterProcess.getItem(i).check)
				    	{
				    		listmarked.add(adapterProcess.getItem(i).processname);
				    	}
				    	
				    }
				    
				    fish.put("processings",listmarked);
				    Log.e("list marked",listmarked.toString());
				    
				    fish.saveInBackground(new SaveCallback() {
						
						@Override
						public void done(ParseException e) {
							if(e==null)
							{
								if(proDialog!=null)
								stopLoading();
							 
							finish();
							Toast.makeText(getApplicationContext(), "Successfully updated the status", Toast.LENGTH_LONG).show();
							}
							else{
								
								if(proDialog!=null)
									stopLoading();
								Toast.makeText(getApplicationContext(), "Unable to Update the fish details now", Toast.LENGTH_LONG).show();
							    
							}
							
						}
					});
				    
				    
				    
				    
				    } else {
				    	if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "Unable to Update the fish details now", Toast.LENGTH_LONG).show();
					    
				      // something went wrong
				    }
				  }
				});
				
			}
			
			
					
				
			
		
	}});
		
		
		
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		switch (requestCode) {
		case SELECT_PHOTO:
			if (resultCode == RESULT_OK) {
				try {
					final Uri imageUri = imageReturnedIntent.getData();

					final InputStream imageStream = getContentResolver()
							.openInputStream(imageUri);
//					Log.e("image", imageStream + "");
				 Bitmap selectedImage = BitmapFactory
							.decodeStream(imageStream);
//					Bitmap selectedimage = Bitmap.createScaledBitmap(
//							selectedImage, 256, 256 * selectedImage.getHeight()
//									/ selectedImage.getWidth(), false);
					
					ExifInterface exif = null;
					try {
					    exif = new ExifInterface(ImageFilePath.getPath(getApplicationContext(), imageUri));
					} catch (IOException e) {
					    e.printStackTrace();
					}  
					int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 
					                                       ExifInterface.ORIENTATION_UNDEFINED);
					
					
				 
				selectedImage=Utilities_Functions.rotateBitmap(selectedImage, orientation);
					
					
					
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					
					selectedImage.compress(Bitmap.CompressFormat.JPEG, 25,stream);
					// byte[] byteArray
					array = stream.toByteArray();
					// array = buffer.array();
					item_img.setImageBitmap(selectedImage);
					

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

			}

		}
	}
	
	
}
