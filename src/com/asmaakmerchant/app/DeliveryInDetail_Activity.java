package com.asmaakmerchant.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DeliveryInDetail_Activity extends Activity {
	
	ArrayAdapter<CartItemClass> contactAdapter;

	ListView list;
	Button btn_getorder;
	ImageButton img_back;
	Button btn_cancel,btn_deliver;
	
	protected ProgressDialog proDialog;

	protected void startLoading() {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage("Please Wait.....");
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	protected void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}
	
	
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)
	.showImageForEmptyUri(R.drawable.ic_launcher)
	 .showImageOnFail(R.drawable.ic_launcher)
	 .showImageOnLoading(R.drawable.ic_launcher)
	.build();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		
		contactAdapter = new ArrayAdapter<CartItemClass>(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = getLayoutInflater().inflate(
							R.layout.cartitem, null);
				}
 
				final CartItemClass it=getItem(position);

				ImageView imageView = (ImageView) convertView
							.findViewById(R.id.img_cartitem_itemimg);
					
					TextView txt_name = (TextView) convertView
							.findViewById(R.id.txt_cartitem_name);
						TextView txt_processingtype=(TextView)convertView.findViewById(R.id.txt_cartitem_cleaning_charge);
						TextView txt_cartitem_subtotal=(TextView)convertView.findViewById(R.id.txt_cartitem_subtotal);
						TextView txt_cartitem_qty=(TextView)convertView.findViewById(R.id.txt_cartitem_quantity);
					//	final TextView edit_qty=(TextView)convertView.findViewById(R.id.txt_cartitem_totalquantity);
						ImageLoader imageLoader = ImageLoader.getInstance();
						
							
						txt_name.setText(it.name);
						txt_processingtype.setText(it.processings);
//						edit_qty.setText(it.quantity+"");
//						edit_qty.setEnabled(false);
//						float subtotal=0;
//						if(it.iscleanRequiered){
//							subtotal=it.cleaning_charge*it.quantity*it.unit_price;
//						}
//						else
//							subtotal=it.quantity*it.unit_price;
//						
						txt_cartitem_subtotal.setText("QR "+it.unit_price);
						
						txt_cartitem_qty.setText("/"+it.quantity+" "+it.uom);
					
					
						
					
					if (it.imgurl != null) {
						imageLoader.displayImage(it.imgurl, imageView,options);

					} else {
						imageView.setImageResource(R.drawable.ic_launcher);
					}
					
				
				return convertView;
			}
		};
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		
		
		setContentView(R.layout.deliveryindetail);
		list = (ListView)findViewById(R.id.listview_deliveryindetail_list);
		list.setAdapter(contactAdapter);
		img_back=(ImageButton)findViewById(R.id.left_arrow);
		
		btn_cancel=(Button)findViewById(R.id.btn_deliveryindetail_cancel);
		btn_deliver=(Button)findViewById(R.id.btn_deliveryindetail_deliver);
		
		
		
		img_back.setOnClickListener(new  OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			finish();	
			}
		});
		
		btn_cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			finish();	
			}
		});
		
		final String addr=getIntent().getStringExtra("address");
		final String obid=getIntent().getStringExtra("obid");
		final String phone=getIntent().getStringExtra("phone");
		final String billAmount=getIntent().getStringExtra("billAmount");
		final boolean isBillPayed=getIntent().getBooleanExtra("isBillPayed", false);
		final String usertype=getIntent().getStringExtra("usertype");
		final int deliver=getIntent().getIntExtra("isDeliver", 0);
		
		if(deliver==0){
			btn_deliver.setText("Ship");
		}
		else if(deliver==1){
			btn_deliver.setText("Deliver");
		}
		else{
			btn_deliver.setVisibility(View.GONE);
		}
		
		
		TextView txt_address = (TextView)findViewById(R.id.txt_deliveryindetail_address);
		txt_address.setText(addr);
		
		TextView txt_phone = (TextView)findViewById(R.id.txt_deliveryindetail_phone);
		txt_phone.setText(phone);
		
		TextView txt_billamount = (TextView) findViewById(R.id.txt_deliveryindetail_billamount);
		txt_billamount.setText("QR "+billAmount);
		
		TextView txt_paymentstatus = (TextView)findViewById(R.id.txt_deliveryindetail_paymentstatus);
		if(isBillPayed)
		txt_paymentstatus.setText("payed");
		else
			txt_paymentstatus.setText("not payed");
		
		TextView txt_usertype = (TextView) findViewById(R.id.txt_deliveryindetail_usertype);
		txt_usertype.setText(usertype);
		
//			Intent i=new Intent(getActivity(), DeliveryInDetail_Activity.class);
//				i.putExtra("obid", cont.obid);
//				i.putExtra("address", cont.address);
//				i.putExtra("phone", cont.contact);
//				i.putExtra("billAmount", cont.billAmount);
//				i.putExtra("isBillPayed", cont.isBillpayed);
//				i.putExtra("usertype", cont.usertype);
//				
			
		
		
		
		
		
		
		
HashMap<String, String> input_param = new HashMap<String, String>();
input_param.put("order_header", obid);
		
		ParseCloud.callFunctionInBackground("viewOrderDetail_Merchant", input_param,
				new FunctionCallback<List<ParseObject>>() {

					@Override
					public void done(List<ParseObject> result,
							ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							for(int i=0;i<result.size();i++){
								ParseObject item=result.get(i);
								ParseObject fish=item.getParseObject("fish_obj");
								CartItemClass cartitclass=new CartItemClass();
								cartitclass.cleaning_charge=Float.parseFloat(fish.getNumber("processing_charge").toString());
								
								try{
								cartitclass.imgurl=fish.getParseFile("fish_img").getUrl();
								}
								catch(Exception e2){
									
								}
								
								cartitclass.iscleanRequiered=item.getBoolean("isCleanRequired");
								cartitclass.name=fish.getString("fish_name");
								cartitclass.obid=item.getObjectId();
								try {
									cartitclass.processings=item.getJSONArray("processings").getString(0);
								} catch (JSONException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								cartitclass.quantity=item.getInt("quantity");
								cartitclass.unit_price=Float.parseFloat(item.getNumber("item_total").toString());
								cartitclass.uom=fish.getString("uom");
								
//								float subtotal=0;
//								if(cartitclass.iscleanRequiered){
//									subtotal=cartitclass.cleaning_charge*cartitclass.quantity*cartitclass.unit_price;
//								}
//								else
//									subtotal=cartitclass.quantity*cartitclass.unit_price;
//							
//								totalprice+=subtotal;
//								
//								
								contactAdapter.add(cartitclass);
							
							}
							
							Utilities_Functions.setListViewHeightBasedOnChildren(list);
							
						}
						else {
							Log.e("error while loading", e.toString());
						}
						
					
					
					}
					
		
		});
		
		
		btn_deliver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			startLoading();
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Order_Header");
				query.getInBackground(obid, new GetCallback<ParseObject>() {
				  public void done(ParseObject object, ParseException e) {
				    if (e == null) {
				   
				   if(deliver==0)
				    object.put("order_status","OnDelivery");
				   else
					   object.put("order_status","Delivered");
				   
				   object.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException e) {

					      if(proDialog!=null)
					    	  stopLoading();
						
						if(e==null){
							Toast.makeText(getApplicationContext(), "successfully changed the status", Toast.LENGTH_LONG).show();
//							if(deliver==0){
//								btn_deliver.setText("Delivered");
//							}
//							else{
//								btn_deliver.setVisibility(View.GONE);
//							}
							
							finish();
						}
						else{
							Log.e("error", e.toString());
							Toast.makeText(getApplicationContext(), "Unable to change the status on delivery", Toast.LENGTH_LONG).show();
						}
						
						
					}
				});
				   
				   
				   
				    
				    } else {
				      if(proDialog!=null)
				    	  stopLoading();
				      Log.e("error", e.toString());
				      Toast.makeText(getApplicationContext(), "Unable to change the status on delivery", Toast.LENGTH_LONG).show();
						
				    }
				  }
				});
				
				
			}
		});
		
		
		
	}

	
}
