package com.asmaakmerchant.app;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PageAdapterDelivery extends FragmentStatePagerAdapter {

	CharSequence Titles[]; // This will Store the Titles of the Tabs which are
							// Going to be passed when ViewPagerAdapter is
							// created
	int NumbOfTabs; // Store the number of tabs, this will also be passed when
					// the ViewPagerAdapter is created

	// Build a Constructor and assign the passed Values to appropriate values in
	// the class
	public PageAdapterDelivery(FragmentManager fm, CharSequence mTitles[],
			int mNumbOfTabsumb) {
		super(fm);

		this.Titles = mTitles;
		this.NumbOfTabs = mNumbOfTabsumb;

	}

	// This method return the fragment for the every position in the View Pager
	@Override
	public Fragment getItem(int position) {

		if (position == 0) // if the position is 0 we are returning the First
							// tab
		{
			NewDelivery_Fragment tab1 = new NewDelivery_Fragment();
			return tab1;
		} 
		else if(position==1)
			
		{
			OnDelivery_Fragment tab2 = new OnDelivery_Fragment();
			return tab2;
		}
		else if(position==2)
		{
			Delivered_Fragment tab3 = new Delivered_Fragment();
			return tab3;
		
		}
		else{
			DeliveryPlaces_Fragment tab4=new DeliveryPlaces_Fragment();
			return tab4;
		}

	}

	// This method return the titles for the Tabs in the Tab Strip

	@Override
	public CharSequence getPageTitle(int position) {
		return Titles[position];
	}

	// This method return the Number of tabs for the tabs Strip

	@Override
	public int getCount() {
		return NumbOfTabs;
	}
}
