package com.asmaakmerchant.app;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {
ParseUser user;
EditText edit_email,edit_password;
Button btn_signin;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		user=ParseUser.getCurrentUser();
	
			
			setContentView(R.layout.login);
			
			btn_signin=(Button)findViewById(R.id.btn_signin_signin);
			edit_email=(EditText)findViewById(R.id.edit_signin_email);
			edit_password=(EditText)findViewById(R.id.edit_signin_password);
			
			
			btn_signin.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					String email,pass;
					email=edit_email.getText().toString();
					pass=edit_password.getText().toString();
					if(email.length()!=0&&pass.length()!=0){
						if(Utilities_Functions.chekEmailId(email)){
						
					if(Utilities_Functions.checkinternet(getApplicationContext())){
					
				ParseUser.logInInBackground(email, pass, new LogInCallback() {
					
					@Override
					public void done(ParseUser userob, ParseException e) {
						// TODO Auto-generated method stub
						if(e==null){
							
						if(userob.getString("user_type").equals("merchant"))
						{
							finish();
							Intent i=new Intent(getApplicationContext(), Home_Activity.class);
							i.putExtra("category", "all");
							startActivity(i);
						}
						else{
							ParseUser.logOut();
							Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
						}
						
						}
						else{
							Toast.makeText(getApplicationContext(), "Some errors on login.Please check and retry", Toast.LENGTH_LONG).show();
							
						}
					}
				});
					}}}}});
						
						
	
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
